<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
	integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
	crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link
	href="https://mdbootstrap.com/previews/docs/latest/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://mdbootstrap.com/previews/docs/latest/css/mdb.min.css"
	rel="stylesheet">
<link href="style.css" rel="stylesheet">

</head>
<body>

<header>
		<nav
			class="navbar navbar-expand-lg navbar-dark default-color-dark fixed-top">
			<a class="navbar-brand" href="home.jsp">Personal Details</a> <a
				class="navbar-brand" href="index.jsp">Qualification Details</a> <a
				class="navbar-brand" href="index.html">Placement Details</a>

		</nav>
	</header>

	<div class="container-fluid">
		<div class="row b" style="height: 80px;"></div>
		<form class="form-horizontal">

			<div class=row>
				<div class="col-sm-4"></div>
				<div class="col-sm-6">
					<fieldset>

						<!-- Form Name -->
						<legend>Qualification Details</legend>

						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="selectbasic">Qualification</label>
							<div class="col-md-4">
								<select id="selectbasic" name="selectbasic" class="form-control">
									<option value="1">SSC</option>
									<option value="2">HSC</option>
									<option value="3">DEGREE</option>
									<option value="4">CDAC</option>
								</select>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">College
								name</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="enter college name" class="form-control input-md">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="ID">University
								Name</label>
							<div class="col-md-4">
								<input id="ID" name="ID" type="text"
									placeholder="Enter University Name"
									class="form-control input-md">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Passing
								Year</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="enter passing year" class="form-control input-md">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Aggrigate</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="enter aggrigate" class="form-control input-md">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">No
								of Backlogs</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="enter backlogs" class="form-control input-md">

							</div>
						</div>

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="singlebutton">Submit</label>
							<div class="col-md-4">
								<button id="singlebutton" name="singlebutton"
									class="btn btn-primary">Submit</button>
							</div>
						</div>


					</fieldset>
				</div>
			</div>
		</form>

		<div class="col-sm-2"></div>
		<footer  style = "position: fixed; left: 0;bottom: 0;width: 100%;  text-align: center"; class="bg-dark text-white">
		<div class="text-center py-2 lead">Copyright � 2018 - DAC SHALA
		</div>
		</footer>
	</div>
	
	
</body>
</html>