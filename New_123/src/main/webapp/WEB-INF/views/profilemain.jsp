<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!--  <html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
</body>
</html>-->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link
	href="https://mdbootstrap.com/previews/docs/latest/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://mdbootstrap.com/previews/docs/latest/css/mdb.min.css"
	rel="stylesheet">
<link href="style.css" rel="stylesheet">
</head>
<body>
	<!--Main Navigation-->
	<header>
		<nav
			class="navbar navbar-expand-lg navbar-dark default-color-dark fixed-top">
			<a class="navbar-brand" href="index.html">Personal Details</a> <a
				class="navbar-brand" href="index.jsp">Qualification Details</a> <a
				class="navbar-brand" href="index.html">Placement Details</a>

		</nav>
	</header>

	<div class="py-2"></div>
	<div class="container-fluid">
		<div class="row mt-5">
			<legend>Placement Information</legend>
		</div>

		<div class="row mt-5">

			<div class="col-sm-6">
				<form class="form-horizontal">
					<fieldset>

						<!-- Form Name -->


						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="">PRN</label>
							<div class="col-sm-6">
								<input id="" name="" type="text" placeholder=""
									class="form-control input-sm" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Course
							</label>
							<div class="col-sm-6">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Total
								Calls</label>
							<div class="col-sm-6">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

					</fieldset>
				</form>




			</div>



			<div class="col-sm-4">
				<form class="form-horizontal">
					<fieldset>

						<!-- Form Name -->


						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="">Name</label>
							<div class="col-sm-6">
								<input id="" name="" type="text" placeholder=""
									class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Batch</label>
							<div class="col-sm-6">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Self
								Placed</label>
							<div class="col-sm-6">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

					</fieldset>
				</form>



			</div>


			
	</div>
	
	<div class="py-2"></div>
	<div class="container-fluid">
		<div class="row mt-5">
			<legend>Contact Information</legend>
		</div>

		<div class="row mt-5">

		<form class="form-horizontal">
<fieldset>

<!-- Form Name -->


<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Address</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="textarea" name="textarea" style="width:300px;height:75px;"></textarea>
  </div>
</div>

</fieldset>
</form>




			<div class="col-sm-4">
				<form class="form-horizontal">
					<fieldset>

						<!-- Form Name -->


						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="">City</label>
							<div class="col-md-4">
								<input id="" name="" type="text" placeholder=""
									class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Country</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Tel No.</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

					</fieldset>
				</form>



			</div>


			<div class="col-sm-4">
				<form class="form-horizontal">
					<fieldset>

						<!-- Form Name -->


						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="">State</label>
							<div class="col-md-4">
								<input id="" name="" type="text" placeholder=""
									class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Pin</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Mobile
							</label>
							<div class="col-md-4">
								<input id="textinput" name="textinput" type="text"
									placeholder="" class="form-control input-md" required="">

							</div>
						</div>

					</fieldset>
				</form>



			</div>





		</div>
	</div>
	

	<footer  style = "position: fixed; left: 0;bottom: 0;width: 100%;  text-align: center"; class="bg-dark text-white">
		<div class="text-center py-2 lead">Copyright � 2018 - DAC SHALA
		</div>
	</footer>
	<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script src="https://mdbootstrap.com/previews/docs/latest/js/bootstrap.min.js"></script>
    <script src="https://mdbootstrap.com/previews/docs/latest/js/mdb.min.js"></script>
    <script src="https://mdbootstrap.com/previews/docs/latest/js/jarallax.js"></script>
    <script src="https://mdbootstrap.com/previews/docs/latest/js/jarallax-video.js"></script>
    <script>
        new WOW().init();
    </script>  -->
</body>
</html>