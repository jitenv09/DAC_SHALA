package com.dac.demo.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.dac.demo.dao.FileUploadDAO;
import com.dac.demo.model.UploadFile;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */

	
	
	
	//first chnge

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "register";
	}
	/*@RequestMapping(value="/register",method=RequestMethod.GET)
	public String register()
	{
		return "register";
	}
<<<<<<< HEAD
	*/
=======
	@Autowired
	private FileUploadDAO fileUploadDaoImpl;

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String showUploadForm(HttpServletRequest request) {
		return "Upload";
	}
	
    @RequestMapping(value = "/doUpload", method = RequestMethod.POST)
    public String handleFileUpload(HttpServletRequest request, @RequestParam CommonsMultipartFile[] fileUpload) throws Exception 
    {
         
        if (fileUpload != null && fileUpload.length > 0) {
            for (CommonsMultipartFile aFile : fileUpload){
                 
                System.out.println("Saving file: " + aFile.getOriginalFilename());
               
                UploadFile uploadFile = new UploadFile();
                uploadFile.setFileName(aFile.getOriginalFilename());
                uploadFile.setData(aFile.getBytes());
                fileUploadDaoImpl.save(uploadFile);                
            }
        }
 
        return "Success";
    }	
    @RequestMapping(value="/loginadmin",method=RequestMethod.GET)
	public String LoginAdmin()
	{
    	return "loginadmin";
	}
    @RequestMapping(value="/loginstudent",method=RequestMethod.GET)
	public String LoginStudent()
	{
    	return "loginstudent";
	}
    @RequestMapping(value="/about",method=RequestMethod.GET)
	public String AboutUs()
	{
    	return "about";
	}
    @RequestMapping(value="/contact",method=RequestMethod.GET)
	public String ContactUs()
	{
    	return "contact";
	}
	
	
	
>>>>>>> branch 'master' of https://gitlab.com/jitenv09/DAC_SHALA.git
}
