package com.dac.demo.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dac.demo.model.Users;

public class UserDaoImpl implements userdao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public boolean saveUser(Users u) {
		// TODO Auto-generated method stub
		sessionfactory.getCurrentSession().save(u);
		return false;
	}

}
