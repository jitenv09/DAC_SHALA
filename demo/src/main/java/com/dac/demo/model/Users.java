package com.dac.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name="Users")
public class Users   {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "userId", unique = true , nullable = false)
	private long userId;
	
	@Column(name = "first_name")
	@NotNull
	private String first_name;
	
	@Column(name = "middle_name")
	@NotNull
	private String middle_name;
	
	@Column(name = "Lastname",nullable =true)
	private String lastname;
	
	@Column(name = "gender")
	@NotNull
	private String gender;
	
	@Column(name = "dob")
	//@NotNull
	private String dob;
	
		
	@Column(name="email",unique=true)
	@NotNull @Email
	private String email;
	
	@Column(name="mobile",unique=true)
	@NotNull
	private String mobile;
	
	@Column(name = "aadhar")
	@NotNull
	private String aadhar;
	
	
	
	@Column(name = "Password")
	@NotNull
	@Size(min=8,max=50,message="Pasword should have min legth between {min} and {max}")
	private String password;
		
	
	/*@OneToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private UserAddress address;*/
	
	
	@Column(name = "isActive")
	private boolean isActive;
	
	@Column(name = "createdAt")
	
	private String createdAt;
	
	@Column(name = "updatedAt")
	private String updatedAt;
	
	
	@ManyToOne
	@JoinColumn(name="userRoleId")
	private Users_Roles userrole;

	
	


	public long getUserId() {
		return userId;
	}




	public void setUserId(long userId) {
		this.userId = userId;
	}




	public String getFirst_name() {
		return first_name;
	}




	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}




	public String getMiddle_name() {
		return middle_name;
	}




	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}




	public String getLastname() {
		return lastname;
	}




	public void setLastname(String lastname) {
		this.lastname = lastname;
	}




	public String getGender() {
		return gender;
	}




	public void setGender(String gender) {
		this.gender = gender;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getMobile() {
		return mobile;
	}




	public void setMobile(String mobile) {
		this.mobile = mobile;
	}




	public String getAadhar() {
		return aadhar;
	}




	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}





	public boolean isActive() {
		return isActive;
	}




	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}




	public String getCreatedAt() {
		return createdAt;
	}




	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}




	public String getUpdatedAt() {
		return updatedAt;
	}




	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}




	public Users_Roles getUserrole() {
		return userrole;
	}




	public void setUserrole(Users_Roles userrole) {
		this.userrole = userrole;
	}






	public String getDob() {
		return dob;
	}




	public void setDob(String dob) {
		this.dob = dob;
	}







}
