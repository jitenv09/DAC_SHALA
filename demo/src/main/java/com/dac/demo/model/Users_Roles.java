package com.dac.demo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="User_Roles")
public class Users_Roles {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "UserRoleId", unique = true, nullable = false)
	private Long userRoleId;
	
	@Column(name = "Role", unique=true)
	private String role;	// party collector Secretary Commissioner
	
	@OneToMany(mappedBy="userrole", cascade=CascadeType.ALL)
	private Set<Users> user; 
	

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getRole() {
		System.out.println("loggedin as :"+role);
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<Users> getUser() {
		return user;
	}

	public void setUser(Set<Users> user) {
		this.user = user;
	}

	

}
